import './App.css';
import Lyrics from './Lyrics';
import partyParrot from './party-parrot.gif'
import Header from './style-wrappers/Header';

function App() {
  return (
    <div className="App" style={{paddingTop: '30px'}}>
      <Header>Mood.</Header>
      <img src={partyParrot} alt="party" style={{position: 'absolute', left: 0, top: 10}} />
      <img src={partyParrot} alt="party" style={{position: 'absolute', right: 0, top: 10 }} />
      <Lyrics />
    </div>
  );
}

export default App;
