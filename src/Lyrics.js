import React, { useState } from 'react';
import styled from 'styled-components';
import Sentiment from 'sentiment';
import * as Tone from 'tone';

import Song from './audio.mp3';
import { seventhChords, scales } from './Const';
import { playBass, playMelody } from './Players';
import partyParrot from './party-parrot.gif'
import rad from './rad.gif';

const Div = styled.div`
  display: flex;
  width: 100vw;
  flex-direction: row;
  margin-top: 30px;
  justify-content: center;
`;

const Button = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid #eef;
  color: #eef;
  margin-left: 1em;
  padding: 0.25em 1em;
  align-self: flex-end;
  font-size: 30px;
  font-family: 'Josefin Sans', sans-serif;
`;

const Textbox = styled.textarea`
  width: 400px;
  font-size: 20px;
  height: 100px;
  background: transparent;
  border: 2px solid #eef;
  color: #eef;
  align-self: flex-end;
  font-family: 'Josefin Sans', sans-serif;
  padding: 0.5em;

  &:focus {
    outline: none;
  }
`;

const Lyrics = () => {

  const [lyrics, setLyrics] = useState("I can't get no satisfaction");
  const [songStyle, setSongStyle] = useState('');
  const [isPartying, setIsPartying] = useState(false);

  const play = () => {
    Tone.Transport.cancel();

    if (lyrics.toLowerCase().includes("party")) {
      setIsPartying(true);
      const player = new Tone.Player(Song).toDestination();
      Tone.loaded().then(() => {
        player.start();
      });
      return;
    }

    const sentiment = new Sentiment();
    const result = sentiment.analyze(lyrics);
    const songLength = lyrics.length

    switch(true) {
      case result.score < -2:
        playBass(seventhChords.diminished, songLength);
        playMelody(scales.diminished, songLength)
        break;
      case result.score < -1:
        playBass(seventhChords.halfDiminished, songLength);
        playMelody(scales.halfDiminished, songLength)
        break;
      case result.score < 0:
        playBass(seventhChords.minor, songLength);
        playMelody(scales.minor, songLength)
        break;
      case result.score < 2:
        playBass(seventhChords.dominant, songLength);
        playMelody(scales.dominant, songLength)
        break;
      default:
        playBass(seventhChords.major, songLength);
        playMelody(scales.major, songLength);
    }

    setSongStyle(`Here's a sentiment level ${result.score} song for ya! It's a whole ${songLength} seconds long...`);
    Tone.Transport.start();
  }
  const stop = () => {Tone.Transport.stop();}

  return (
    <div>
      <Div>
        <img src={partyParrot} alt="party" style={{ position: 'absolute', left: 0, top: 10, visibility: `${isPartying ? 'hidden' : 'visible'}` }} />
        <img src={partyParrot} alt="party" style={{ position: 'absolute', right: 0, top: 10, visibility: isPartying ? 'hidden' :'visible' }} />
        <img src={rad} alt="party" width="498px" height='498px' style={{ position: 'absolute', left: 0, top: 10, visibility: isPartying ? 'visible' : 'hidden'} } />
        <img src={rad} alt="party" width="498px" height='498px' style={{ position: 'absolute', right: 0, top: 10, visibility: `${!isPartying ? 'hidden' : 'visible'}` }} />

        <Textbox onInput={(evt) => { setLyrics(evt.target.value) }} value={lyrics}/>
        <Button onClick={play}>Moodify me</Button>
        {isPartying ? <div /> : <Button onClick={stop}>Nah</Button> }
      </Div>
      <div style={{ marginTop: '30px', width: '100%'}}>
        <span style={{ fontSize: '20px', color: '#eef' }}><marquee scrollamount="15">{isPartying ? "NEVER GONNA GIVE YOU UP ... NEVER GONNA LET YOU DOWN" : songStyle}</marquee></span>
      </div>
    </div>
  )
}

export default Lyrics;
