import styled from 'styled-components';
import * as Tone from 'tone';

const WhiteKeyDiv = styled.div`
  width: 50px;
  height: 250px;
  background-color: #fff;
  border: 1px solid #000;
`;

const synth = new Tone.Synth().toDestination();

const WhiteKey = () => {
  const clickHandler = () => {
    const now = Tone.now()
    // trigger the attack immediately
    synth.triggerAttack("C4", now)
    // wait one second before triggering the release
    synth.triggerRelease(now + 1)
  }

  return (
    <WhiteKeyDiv onClick={clickHandler}/>
  )
}

export default WhiteKey;
