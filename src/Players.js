import * as Tone from 'tone';

export const playChord = (now, chord, offset = 0, ) => {
  const synth = new Tone.PolySynth(Tone.Synth).toDestination();

  const playFor = 1;
  chord.forEach((note, index) => {
    synth.triggerAttack(note, now + offset + (index * 0.2));
  });
  synth.triggerRelease(chord, now + offset + playFor);

  return playFor
};

export const playBass = (chord, songLength) => {
  const synth = new Tone.Synth(Tone.PluckSynth).toDestination();

  new Tone.Loop(time => {
    const chosenValue = Math.random();
    let note;
    switch (true) {
      case chosenValue < 0.5:
        note = chord[0];
        break;
      case chosenValue < 0.666:
        note = chord[1];
        break;
      case chosenValue < 0.833:
        note = chord[2];
        break;
      default:
        note = chord[3];
    }

    synth.triggerAttackRelease(note, "2n", time);
    console.log('looping', note);
  }, "2n").start(0).stop(songLength);
}

export const playMelody = (scale, songLength) => {
  const synth = new Tone.Synth(Tone.FMSynth).toDestination();

  new Tone.Loop(time => {
    const chosenValue = Math.random();
    let note;
    switch (true) {
      case chosenValue < 0.25:
        note = scale[0];
        break;
      case chosenValue < 0.36:
        note = scale[1];
        break;
      case chosenValue < 0.47:
        note = scale[2];
        break;
      case chosenValue < 0.58:
        note = scale[3];
        break;
      case chosenValue < 0.69:
        note = scale[4];
        break;
      case chosenValue < 0.8:
        note = scale[5];
        break;
      case chosenValue < 0.91:
        note = scale[6];
        break;
      default:
        note = scale[7];
    }

    synth.triggerAttackRelease(note, "4n", time);
    console.log('looping', note);
  }, "4n").start(0).stop(songLength);
}

// export const playRandomNotesFromNoteCollection = (noteCollection) => {
//   // create two monophonic synths
//   const synthA = new Tone.FMSynth().toDestination();
//   const synthB = new Tone.AMSynth().toDestination();


//   //play a note every quarter-note
//   const loopA = new Tone.Loop(time => {
//     let interval;
//     const chosenValue = Math.random();
//     switch(true) {
//       case chosenValue < 0.33:
//         interval = "4n";
//         break;
//       case chosenValue < 0.5:
//         interval = "2n";
//         break;
//       case chosenValue < 0.75:
//         interval = "8n";
//         break;
//       default:
//         interval = ""
//     }
//     synthA.triggerAttackRelease("C2", chosenValue, time);
//   }, "4n").start(0);

//   //play another note every off quarter-note, by starting it "8n"
//   const loopB = new Tone.Loop(time => {
//     synthB.triggerAttackRelease("C4", "8n", time);
//   }, "4n").start("8n");

//   // the loops start when the Transport is started

// }
