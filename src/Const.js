export const seventhChords = {
  dominant: ["F#2", "A#2", "C#3", "E3"],
  major: ["G3", "B3", "D3", "F#3"],
  minor: ["C3", "E3", "G3", "B3"],
  halfDiminished: ["B2", "D3", "F3", "A3"],
  diminished: ["C3", "Eb3", "Gb3", "A3"]
};

export const scales = {
  dominant: ["F#3", "G#3", "A#3", "B3", "C#4", "D#4", "E#4", "F4"],
  major: ["G3", "A3", "B3", "C4", "D4", "E4", "F#4", "G4"],
  minor: ["C4", "D4", "E4", "F4", "G4", "A4", "B4","C5"],
  halfDiminished: ["B3", "C4", "D4", "E4", "F4", "G4", "A4", "B4" ],
  diminished: ["C4", "D4", "Eb4", "F4", "Gb4", "Ab4", "A4", "B4"],
}