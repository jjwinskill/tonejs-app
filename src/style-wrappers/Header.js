import styled from 'styled-components';

export default styled.h1`
  font-family: 'Pacifico', cursive;
  width: auto;
  margin-top: 30px;
  font-size: 63px;
  color: #eef;
  text-align: center;
`;